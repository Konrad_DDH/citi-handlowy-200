module.exports = function(grunt) {

	grunt.initConfig({
		uglify: {
	    	my_target: {
	    		files: {
	        		'js/scripts.min.js': ['js/jquery-3.1.1.min.js']
	      		}
	    	}
	  	},
	  	
  		sass: {
     		options: {
            	sourceMap: true,
            	outputStyle: "compressed",
        	},
        	dist: {
            	files: {
                	'css/main.min.css': 'css/sass/main.scss'
            	}
        	}
    	}
	});
	
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.registerTask('default', ['sass', 'uglify']);
};